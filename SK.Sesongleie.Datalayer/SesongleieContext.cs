﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using SK.Sesongleie.DomainClasses;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace SK.Sesongleie.Datalayer
{
    public class SesongleieContext : DbContext
    {
        public DbSet<Leieobjekt> LeieObjekter { get; set; }
        public DbSet<Leiesoknad> LeieSoknader { get; set; }
        public DbSet<Leiespesifikasjon> LeieSpesifikasjoner { get; set; }
        public DbSet<Leietaker> LeieTakere { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            modelBuilder.Configurations.Add(new LeieobjektConfiguration());
            
            base.OnModelCreating(modelBuilder);
        }

    }

    //Definerer endringer som gjøres i modelbuilder vha. fluentAPI. Eks. sette fremmednøkler, gi nytt navn til felter etc.
    //Dersom mange endringer kan de deles opp i entitytypeconfiguration klasser.
    public class LeieobjektConfiguration : EntityTypeConfiguration<Leieobjekt>
    {
        public LeieobjektConfiguration()
        {
            Property(c => c.Navn).HasColumnName("Leieobjektnavn");
        }
    }
}
