namespace SK.Sesongleie.Datalayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DisablePluralizingTableNames : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Leieobjekts", newName: "Leieobjekt");
            RenameTable(name: "dbo.Leiespesifikasjons", newName: "Leiespesifikasjon");
            RenameTable(name: "dbo.Leiesoknads", newName: "Leiesoknad");
            RenameTable(name: "dbo.Leietakers", newName: "Leietaker");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.Leietaker", newName: "Leietakers");
            RenameTable(name: "dbo.Leiesoknad", newName: "Leiesoknads");
            RenameTable(name: "dbo.Leiespesifikasjon", newName: "Leiespesifikasjons");
            RenameTable(name: "dbo.Leieobjekt", newName: "Leieobjekts");
        }
    }
}
