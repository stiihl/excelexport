namespace SK.Sesongleie.Datalayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialCreate : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Leieobjekts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Navn = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Leiespesifikasjons",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Leiedag = c.String(),
                        Fra = c.DateTime(nullable: false),
                        Til = c.DateTime(nullable: false),
                        Under17 = c.Boolean(nullable: false),
                        LeiesoknadId = c.Int(nullable: false),
                        LeieobjektId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Leieobjekts", t => t.LeieobjektId, cascadeDelete: true)
                .ForeignKey("dbo.Leiesoknads", t => t.LeiesoknadId, cascadeDelete: true)
                .Index(t => t.LeiesoknadId)
                .Index(t => t.LeieobjektId);
            
            CreateTable(
                "dbo.Leiesoknads",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Referanse = c.String(),
                        Registrert = c.DateTime(nullable: false),
                        LeietakerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Leietakers", t => t.LeietakerId, cascadeDelete: true)
                .Index(t => t.LeietakerId);
            
            CreateTable(
                "dbo.Leietakers",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Orgnr = c.String(),
                        Forening = c.String(),
                        Adresse = c.String(),
                        PostNr = c.String(),
                        PostAdr = c.String(),
                        Telefon = c.String(),
                        Epost = c.String(),
                        Fornavn = c.String(),
                        Etternavn = c.String(),
                        LagTom17 = c.Int(nullable: false),
                        LagOver17 = c.Int(nullable: false),
                        MedlemNIF = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Leiesoknads", "LeietakerId", "dbo.Leietakers");
            DropForeignKey("dbo.Leiespesifikasjons", "LeiesoknadId", "dbo.Leiesoknads");
            DropForeignKey("dbo.Leiespesifikasjons", "LeieobjektId", "dbo.Leieobjekts");
            DropIndex("dbo.Leiesoknads", new[] { "LeietakerId" });
            DropIndex("dbo.Leiespesifikasjons", new[] { "LeieobjektId" });
            DropIndex("dbo.Leiespesifikasjons", new[] { "LeiesoknadId" });
            DropTable("dbo.Leietakers");
            DropTable("dbo.Leiesoknads");
            DropTable("dbo.Leiespesifikasjons");
            DropTable("dbo.Leieobjekts");
        }
    }
}
