namespace SK.Sesongleie.Datalayer.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeColumnNameInLeieobjektFromNameToLeieobjektnavn : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.Leieobjekt", name: "Navn", newName: "Leieobjektnavn");
        }
        
        public override void Down()
        {
            RenameColumn(table: "dbo.Leieobjekt", name: "Leieobjektnavn", newName: "Navn");
        }
    }
}
