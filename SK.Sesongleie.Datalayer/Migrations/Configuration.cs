namespace SK.Sesongleie.Datalayer.Migrations
{
    using SK.Sesongleie.DomainClasses;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    public sealed class Configuration : DbMigrationsConfiguration<SK.Sesongleie.Datalayer.SesongleieContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "SK.Sesongleie.Datalayer.SesongleieContext";
        }

        protected override void Seed(SesongleieContext context)
        {
            // legger til debugdirektiv slik at seed koden ikke skal kj�re hver gang.
#if false
            var leietakere = new List<Leietaker>
            {
                new Leietaker
                {
                    Forening = "Testforening" //TODO: Fyll ut listen
                }
            };

            context.LeieTakere.AddOrUpdate(
                c => new { c.Id, c.Forening }, leietakere.ToArray());
#endif
        }
    }
}
