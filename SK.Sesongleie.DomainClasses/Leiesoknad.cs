﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Sesongleie.DomainClasses
{
    public class Leiesoknad
    {
        public int Id { get; set; }
        public int LeietakerId { get; set; }
        public string Referanse { get; set; }
        public DateTime Registrert { get; set; }
    }
}
