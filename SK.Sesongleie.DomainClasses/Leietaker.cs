﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Sesongleie.DomainClasses
{
    public class Leietaker
    {
        public int Id { get; set; }
        public string Orgnr { get; set; }
        public string Forening { get; set; }
        public string Adresse { get; set; }
        public string PostNr { get; set; }
        public string PostAdr { get; set; }
        public string Telefon { get; set; }
        public string Epost { get; set; }
        public string Fornavn { get; set; }
        public string Etternavn { get; set; }
        public int LagTom17 { get; set; }
        public int LagOver17 { get; set; }
        public bool MedlemNIF { get; set; }
    }
}
