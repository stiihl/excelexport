﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Sesongleie.DomainClasses
{
    public class Leieobjekt
    {
        public int Id { get; set; }
        public string Navn { get; set; }
    }
}
