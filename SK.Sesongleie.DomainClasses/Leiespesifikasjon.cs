﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Sesongleie.DomainClasses
{
    public class Leiespesifikasjon
    {
        public int Id { get; set; }
        public int LeiesoknadId { get; set; }
        public int LeieobjektId { get; set; }
        public string Leiedag { get; set; }
        public DateTime Fra { get; set; }
        public DateTime Til { get; set; }
        public bool Under17 { get; set; }        
    }
}
