﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SK.Sesongleie.Model;


namespace SK.Sesongleie
{    
    public class ExcelReport : Excel
    {
        public ExcelReport(string filePath)
            : base(filePath)
        { }

        
        public bool WriteOnsker(List<Leieobjekt> leieObjekter)
        {
            const int firstColumn = 2;
            const int lastColumn = 6;
            const int headerRow = 3;
            const int firstRow = 4;
            
            string[] tableHeaders = 
            { 
                "Lag", 
                "Dag", 
                "Fra", 
                "Til", 
                "Timer" 
            };

            try
            {
                // Sjekk om filen er i bruk.
                if (_package == null)
                {
                    throw new Exception(
                        "ExcelPackage = null. Kontroller om filen det skal skrives til allerede er åpen");
                }

                // Løper gjennom leieobjektene og skriver informasjon til excel-fil for hvert leieobjekt.
                foreach (var leieObjekt in leieObjekter)
                {
                    // Tilordne til arkfane med navnet på leieobjektet.
                    Worksheet = Package.Workbook.Worksheets[leieObjekt.Navn];

                    if (Worksheet == null)
                    {   // Oppretter arkfanen dersom den ikke finnes.
                        Worksheet = Package.Workbook.Worksheets.Add(leieObjekt.Navn);
                    }

                    // Skriv navnet på hallen uthevet i cellen 'B2';
                    Worksheet.Cells["B2"].Value = leieObjekt.Navn;
                    Worksheet.Cells["B2"].Style.Font.Bold = true;

                    // Opprett tabellheader og stil tabellen
                    Worksheet.Cells[headerRow, firstColumn + (int)TableHeader.LAG].Value = 
                        tableHeaders[(int)TableHeader.LAG];                    
                    Worksheet.Cells[headerRow, firstColumn + (int)TableHeader.DAG].Value = 
                        tableHeaders[(int)TableHeader.DAG];
                    Worksheet.Cells[headerRow, firstColumn + (int)TableHeader.FRA].Value = 
                        tableHeaders[(int)TableHeader.FRA];
                    Worksheet.Cells[headerRow, firstColumn + (int)TableHeader.TIL].Value = 
                        tableHeaders[(int)TableHeader.TIL];
                    Worksheet.Cells[headerRow, firstColumn + (int)TableHeader.TIMER].Value = 
                        tableHeaders[(int)TableHeader.TIMER];                                                            

                    // Sett farge på header
                    using (var range = Worksheet.Cells[headerRow, firstColumn, headerRow, lastColumn])
                    {
                        range.Style.Font.Bold = true;
                        range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                        // Setting background color
                        range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                        // Setting font color.
                        range.Style.Font.Color.SetColor(Color.Black);
                    }

                    // Gi inn verdier i tabellen.
                    var rowOffset = 0;

                    foreach (var onske in leieObjekt.Onsker)
                    {
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.LAG].Value = onske.Foreningsnavn;
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.DAG].Value = onske.Dag;
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.FRA].Style.Numberformat.Format = "hh:mm";
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.FRA].Value = onske.Fra;
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.TIL].Style.Numberformat.Format = "hh:mm";
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.TIL].Value = onske.Til;
                        Worksheet.Cells[firstRow + rowOffset, firstColumn + (int)TableHeader.TIMER].Value = onske.Timer;
                        rowOffset++;
                    }
                    // Vis all tekst i cellene.
                    Worksheet.Cells.AutoFitColumns(0);
                }
                
                Save();
                return true;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Something went wrong: {0}", ex.Message);
                return false;
            }            
        }
    }
}
