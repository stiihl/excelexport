﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Sesongleie
{
    public class Excel
    {
        private string _filePath;
        private FileInfo _excelFile;
        protected ExcelPackage _package;        

        //constructor
        public Excel(string filePath)
        {
            _filePath = filePath;
            _excelFile = new FileInfo(_filePath);
            _package = CreateExcelPackage(_excelFile);                
        }

        public ExcelPackage Package 
        {
            get { return _package; }
        }

        public FileInfo ExcelFile 
        {
            get { return _excelFile; } 
        }

        public ExcelWorksheet Worksheet { get; set; }
        
        public void Save()
        {
            _package.Save();
        }

        /// <summary>
        /// Metoden CreateExcelPackage
        /// Private hjelpemetode.
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        private ExcelPackage CreateExcelPackage(FileInfo file)
        {            
            try 
            { 
                return new ExcelPackage(file);
            }
            catch
            {
                return null;
            }            
        }
    }
}
