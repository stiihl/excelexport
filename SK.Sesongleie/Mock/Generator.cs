﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SK.Sesongleie.Model;

namespace SK.Sesongleie.Mock
{
    public static class Generator
    {
        public static List<Leieobjekt> CreateHallOnsker()
        {
            List<Leieobjekt> objekter = new List<Leieobjekt>();


            var leieObjekt = new Leieobjekt
            {
                Navn = "Skedsmohallen",
                Onsker = new List<Onske>
                {
                    new Onske
                    {
                        Foreningsnavn = "Skedsmo ball",
                        Dag = "mandag",
                        Fra = Convert.ToDateTime("17:00"),
                        Til = Convert.ToDateTime("18:00"),
                        Timer = 1.0
                    },
                    new Onske
                    {
                        Foreningsnavn = "Skedsmo dans",
                        Dag = "mandag",
                        Fra = Convert.ToDateTime("19:00"),
                        Til = Convert.ToDateTime("20:30"),
                        Timer = 1.0
                    },
                    new Onske
                    {
                        Foreningsnavn = "Skedsmo håndball",
                        Dag = "tirsdag",
                        Fra = Convert.ToDateTime("18:00"),
                        Til = Convert.ToDateTime("19:30"),
                        Timer = 1.0
                    },
                    new Onske
                    {
                        Foreningsnavn = "Strømmen øreristing",
                        Dag = "onsdag",
                        Fra = Convert.ToDateTime("19:00"),
                        Til = Convert.ToDateTime("21:00"),
                        Timer = 1.0
                    }
                }

            };
            objekter.Add(leieObjekt);

            leieObjekt = new Leieobjekt
            {
                Navn = "Asak skole",
                Onsker = new List<Onske>
                {
                    new Onske{
                        Foreningsnavn = "Asak dans",
                        Dag = "tirsdag",
                        Fra = Convert.ToDateTime("19:00"),
                        Til = Convert.ToDateTime("20:30"),
                        Timer = 1.0
                    },
                      new Onske{
                        Foreningsnavn = "Leira romperisterlag",
                        Dag = "onsdag",
                        Fra = Convert.ToDateTime("21:00"),
                        Til = Convert.ToDateTime("22:30"),
                        Timer = 1.0
                    },
                     new Onske{
                        Foreningsnavn = "Asak trommegutter",
                        Dag = "onsdag",
                        Fra = Convert.ToDateTime("16:00"),
                        Til = Convert.ToDateTime("18:00"),
                        Timer = 1.0
                    }
                }

            };



            objekter.Add(leieObjekt);
            return objekter;
        }
    }
}
