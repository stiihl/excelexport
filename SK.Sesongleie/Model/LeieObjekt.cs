﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SK.Sesongleie.Model
{
    public class Leieobjekt
    {
        public string Navn { get; set; }
        public List<Onske> Onsker { get; set; }
    }
}
