﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace EE.console
{
 
    public static class Eksport
    {
        public static List<HallOnske> CreateHallOnsker()
        {
            List<HallOnske> onsker = new List<HallOnske>();

            
            var onske = new HallOnske
            {
                Navn = "Skedsmohallen",
                leieSpesifikasjon = new List<LeieSpesifikasjon>
                {
                    new LeieSpesifikasjon
                    {
                        Foreningsnavn = "Skedsmo ball",
                        Dag = "mandag",
                        Fra = Convert.ToDateTime("17:00"),
                        Til = Convert.ToDateTime("18:00"),
                        Timer = 1.0
                    },
                    new LeieSpesifikasjon
                    {
                        Foreningsnavn = "Skedsmo dans",
                        Dag = "mandag",
                        Fra = Convert.ToDateTime("19:00"),
                        Til = Convert.ToDateTime("20:30"),
                        Timer = 1.0
                    },
                    new LeieSpesifikasjon
                    {
                        Foreningsnavn = "Skedsmo håndball",
                        Dag = "tirsdag",
                        Fra = Convert.ToDateTime("18:00"),
                        Til = Convert.ToDateTime("19:30"),
                        Timer = 1.0
                    },
                    new LeieSpesifikasjon
                    {
                        Foreningsnavn = "Strømmen øreristing",
                        Dag = "onsdag",
                        Fra = Convert.ToDateTime("19:00"),
                        Til = Convert.ToDateTime("21:00"),
                        Timer = 1.0
                    }
                }            

            };
            onsker.Add(onske);

            onske = new HallOnske
            {
                Navn = "Asak skole",
                leieSpesifikasjon = new List<LeieSpesifikasjon>
                {
                    new LeieSpesifikasjon{
                        Foreningsnavn = "Asak dans",
                        Dag = "tirsdag",
                        Fra = Convert.ToDateTime("19:00"),
                        Til = Convert.ToDateTime("20:30"),
                        Timer = 1.0
                    },
                      new LeieSpesifikasjon{
                        Foreningsnavn = "Leira romperisterlag",
                        Dag = "onsdag",
                        Fra = Convert.ToDateTime("21:00"),
                        Til = Convert.ToDateTime("22:30"),
                        Timer = 1.0
                    },
                     new LeieSpesifikasjon{
                        Foreningsnavn = "Asak trommegutter",
                        Dag = "onsdag",
                        Fra = Convert.ToDateTime("16:00"),
                        Til = Convert.ToDateTime("18:00"),
                        Timer = 1.0
                    }
                }

            };
            

          
            onsker.Add(onske);
            return onsker;
        }
    }
}
