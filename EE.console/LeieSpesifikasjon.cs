﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.console
{    
    public class LeieSpesifikasjon
    {
        public string Foreningsnavn { get; set; }
        public string Dag { get; set; }
        public DateTime Fra { get; set; }
        public DateTime Til { get; set; }
        public double Timer { get; set; }
    }
}
