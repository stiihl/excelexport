﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OfficeOpenXml;
using SK.Sesongleie;
using SK.Sesongleie.Model;
using SK.Sesongleie.Mock;
using System.Linq;
using SK.Sesongleie.Datalayer;
using SK.Sesongleie.Datalayer.Migrations;
using System.Data.Entity;

namespace EE.console
{
    class Program
    {
        static void Main(string[] args)
        {
            Database.SetInitializer(
                new MigrateDatabaseToLatestVersion<SesongleieContext, Configuration>());

            //EPPlusStartup();
            //WriteToExcel();


            //CreateDatabase();
            HentLeieobjekter();

        }

        private static void CreateDatabase()
        {
            var context = new SesongleieContext();
            context.Database.Initialize(true);
        }


        private static void HentLeieobjekter()
        {
            using(var context = new SesongleieContext())
            {
                var leieobjekter = context.LeieObjekter.ToList();

                foreach (var objekt in leieobjekter)
                {
                    Console.WriteLine(objekt.Navn);
                }
            }
        }

        private static void WriteToExcel()
        {
            ExcelReport report = new ExcelReport(@"C:\Users\stiihl\Docs\Sample1.xlsx");
            List<Leieobjekt> objekter = new List<Leieobjekt>();
            objekter = Generator.CreateHallOnsker();
            var writeOk = report.WriteOnsker(objekter);
            if (writeOk)
                Console.WriteLine("Excelfilen er skrevet.");
            Console.Read();
        }

        private static void EPPlusStartup()
        {
            try
            { 
                //Opprett nytt arbeidsbok.            
                FileInfo newFile = new FileInfo(@"C:\Users\stiihl\Docs\Sample1.xlsx");
                ExcelPackage package = new ExcelPackage(newFile);
                const int firstColumn = 2;
                const int lastColumn = 6;
                const int firstRow = 4;
                //ExcelWorksheet worksheet = package.Workbook.Worksheets["Eksport"];

                //if (worksheet != null)
                //{
                    /*
                     * Input: List<data>
                     * Opprette modell og legge inn mock-data
                     * 
                     * Starte i excel-arket på Cells[1,2]
                     * Lage heading
                     * Løpe gjennom items i listen og legge inn informasjon
                     * Sette rammer
                     * Beregne timer.
                     * 
                     */
                    
                    //oppretter leiedata
                    List<HallOnske> hallOnsker = Eksport.CreateHallOnsker();

                    // løper gjennom hallene
                    foreach (var hallOnske in hallOnsker)
                    {
                        //Opprett arkfane med navnet på hallen.
                        ExcelWorksheet worksheet = package.Workbook.Worksheets[hallOnske.Navn];
                        if(worksheet == null)
                            worksheet = package.Workbook.Worksheets.Add(hallOnske.Navn);

                        //Skriv navnet på hallen i bold til cellen 'B2'.
                        worksheet.Cells["B2"].Value = hallOnske.Navn;
                        worksheet.Cells["B2"].Style.Font.Bold = true;

                        //Opprett tabellheader og stil tabellen
                        worksheet.Cells[3, firstColumn].Value = "Lag";
                        worksheet.Cells[3, firstColumn + 1].Value = "Dag";
                        worksheet.Cells[3, firstColumn + 2].Value = "Fra";
                        worksheet.Cells[3, firstColumn + 3].Value = "Til";
                        worksheet.Cells[3, firstColumn + 4].Value = "Timer";

                        //Sett farge på header
                        using (var range = worksheet.Cells[3, 2, 3, 6])
                        {
                            range.Style.Font.Bold = true;
                            range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            // Setting background color
                            range.Style.Fill.BackgroundColor.SetColor(Color.LightGray);
                            // Setting font color
                            range.Style.Font.Color.SetColor(Color.Black);

                        }

                        var leieSpesifikasjon = hallOnske.leieSpesifikasjon;
                        
                        int rowOffset = 0;
                        
                        //løper gjennom ønskene for aktuell hall
                        foreach (var item in leieSpesifikasjon)
                        {
                            //Skriv inn ønsker for hver forening
                            //worksheet.Cells[X,Y].Value = "Verdi";                            
                            worksheet.Cells[firstRow + rowOffset, firstColumn].Value = item.Foreningsnavn;
                            worksheet.Cells[firstRow + rowOffset, firstColumn + 1].Value = item.Dag;
                            worksheet.Cells[firstRow + rowOffset, firstColumn + 2].Style.Numberformat.Format = "hh:mm";
                            worksheet.Cells[firstRow + rowOffset, firstColumn + 2].Value = item.Fra;
                            worksheet.Cells[firstRow + rowOffset, firstColumn + 3].Style.Numberformat.Format = "hh:mm";
                            worksheet.Cells[firstRow + rowOffset, firstColumn + 3].Value = item.Til;
                            worksheet.Cells[firstRow + rowOffset, lastColumn].Value = item.Timer;
                            

                            //legg bakgrunnsfarge på cellene basert på dag
                            //using (var range = worksheet.Cells[firstRow + rowOffset, firstColumn, firstRow + rowOffset, lastColumn])
                            //{
                            //    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                            //    switch (item.Dag.ToUpper())
                            //    {
                            //        case "MANDAG":
                            //            range.Style.Fill.BackgroundColor.SetColor(Color.Snow);
                            //            break;
                            //        case "TIRSDAG":
                            //            range.Style.Fill.BackgroundColor.SetColor(Color.Honeydew);
                            //            break;
                            //        case "ONSDAG":
                            //            range.Style.Fill.BackgroundColor.SetColor(Color.MintCream);
                            //            break;
                            //        case "TORSDAG":
                            //            range.Style.Fill.BackgroundColor.SetColor(Color.Azure);
                            //            break;
                            //        case "FREDAG":
                            //            range.Style.Fill.BackgroundColor.SetColor(Color.AliceBlue);
                            //            break;
                            //        default:
                            //            range.Style.Fill.BackgroundColor.SetColor(Color.White);
                            //            break;
                            //    }                            
                            //}

                            rowOffset++;

                        } // foreach (var item in leieSpesifikasjon)

                        //Autofit cells
                        worksheet.Cells.AutoFitColumns(0);

                    } // foreach (var hallOnske in hallOnsker)

                    
                    

                    //Console.WriteLine(worksheet.Cells["A1"].Value.ToString());

                    //worksheet.Cells[1, 1].Value = "ID";
                    //worksheet.Cells[1, 2].Value = "Product";
                    //worksheet.Cells[1, 3].Value = "Quantity";
                    //worksheet.Cells[1, 4].Value = "Price";
                    //worksheet.Cells[1, 5].Value = "Value";

                    //worksheet.Cells["A2"].Value = 12001;
                    //worksheet.Cells["B2"].Value = "Nails";
                    //worksheet.Cells["C2"].Value = 37;
                    //worksheet.Cells["D2"].Value = 3.99;

                    //worksheet.Cells["E2:E4"].Formula = "C2*D2";

                    //worksheet.Cells[5, 3, 5, 5].Formula = string.Format
                    //("SUBTOTAL(9, {0})", new ExcelAddress(2, 3, 4, 3).Address);

                    //// Formatting style of the header
                    //using (var range = worksheet.Cells[1, 1, 1, 5])
                    //{
                    //    // Setting bold font
                    //    range.Style.Font.Bold = true;
                    //    // Setting fill type solid
                    //    range.Style.Fill.PatternType = OfficeOpenXml.Style.ExcelFillStyle.Solid;
                    //    // Setting background color dark blue
                    //    range.Style.Fill.BackgroundColor.SetColor(Color.DarkBlue);
                    //    // Setting font color
                    //    range.Style.Font.Color.SetColor(Color.White);
                    //}
               // }
                //else
                //{
                    //Opprette nytt arbeidsark til den tomme arbeidsboken.
                  //  worksheet = package.Workbook.Worksheets.Add("Eksport");
                   // Console.WriteLine("xlsx opprettet.");
                //}

                package.Save();
            }
            catch(Exception ex)
            {
                Console.WriteLine("Something went wrong..{0}", ex.Message);
            }
        }
    }
}
