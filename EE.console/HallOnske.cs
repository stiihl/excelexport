﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EE.console
{
    public class HallOnske
    {
        public string Navn { get; set; }
        public List<LeieSpesifikasjon> leieSpesifikasjon { get; set; }
    }
}
